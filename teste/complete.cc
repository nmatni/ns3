/**
 * =====================================================================================
 *
 *       @file  	complete-network-example.cc
 *
 *       @brief    	This script simulates a complex scenario with multiple gateways and end
 *                  devices. The metric of interest for this script is the throughput of the
 *                  network.
 *       @version  	1.0
 *       @date  	05/23/19 14:17:55
 *       @author  	Nagib Matni, nmatni at gmail dot com
 *       @pre       First initialize the system.
 *       @bug       Not all memory is freed when deleting an object of this class.
 *       @warning   Improper use can crash your application
 *       @copyright GNU Public License.
 *
 * =====================================================================================
 */

#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/end-device-lora-mac.h"
#include "ns3/gateway-lora-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/node-container.h"
#include "ns3/mobility-helper.h"
#include "ns3/position-allocator.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/network-server-helper.h"
#include "ns3/correlated-shadowing-propagation-loss-model.h"
#include "ns3/building-penetration-loss.h"
#include "ns3/building-allocator.h"
#include "ns3/buildings-helper.h"
#include "ns3/forwarder-helper.h"
#include <algorithm>
#include <ctime>
//
#include <ns3/okumura-hata-propagation-loss-model.h>
#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/simulator.h>
#include <iostream>
#include <fstream>
#include <string>
#include<stdlib.h>
//Netamin Module
//#include <ns3/netanim-module.h>


using namespace ns3;
using namespace std;
using namespace lorawan;

NS_LOG_COMPONENT_DEFINE ("ComplexLorawanNetworkExample");

// Variables
bool FromFile = true; /*!< True if the positions is from a File */
// string file_users = "Sim3/User_1000_Sim0.csv"; /*!< File that set *X* and *Y* of the users */
// string file_gateway = "Sim3/gw_1000_Best_Sim1.csv"; /*!< x (tab) y */
//string file_gateway = "gw1.txt";

// Network settings
int nDevices = 5;
int nGateways = 1;
double radius = 2000;
double simulationTime = 600;

// Channel model
bool realisticChannelModel = false;
int appPeriodSeconds = 600;
int periodsToSimulate = 1;
int transientPeriods = 0;

// Output control
bool print = true;
uint32_t randomSeed = 1234;
uint32_t nRuns = 1;

//Changing to Square 
double axis_x = 10000;
double axis_y = 10000;

/**
 *        @name   getFileSize
 *       @brief   Read line by line and increment to calculate the number of line
 *       @param   std::string &fileName Name_of_File
 *        @date   05/23/19
 *        @todo   None 
 *         @bug   None
 *      @return  (int) how many lines the file has
 */
int getFileSize(const std::string &fileName)
{
	int numLines = 0;
	std::string unused;
	ifstream file(fileName.c_str());
	while ( std::getline(file, unused) )
		++numLines;
	return numLines;
}

/**
 * @name main (int argc, char *argv[])
 * @brief aram ui16_Address - First address to be read
 * @param ui16_Len - How many bytes to read
 * @todo None
 * @bug None
 * @return how many bytes it could write
 */
int main (int argc, char *argv[])
{

	/**
	 * Set Values for specific variables from waf 
	 * ------------------------------------------
	 * @param var
	 * @parblock
	 *  Set a Variable use cmd.AddValue ("var", "Number of end devices to include in the simulation", var);
	 *
	 *  ./waf --run "Complete-network-example var=X"
	 * @endparblock
	 */
	CommandLine cmd;
	cmd.AddValue ("nDevices",
								"Number of end devices to include in the simulation",
								nDevices);
	cmd.AddValue ("radius",
								"The radius of the area to simulate",
								radius);
	cmd.AddValue ("simulationTime",
								"The time for which to simulate",
								simulationTime);
	cmd.AddValue ("file_users",
								"Name of User's File",
								file_users);
	cmd.AddValue ("file_gateway",
								"Name of Gateway's File",
								file_gateway);
	cmd.AddValue ("appPeriod",
								"The period in seconds to be used by periodically transmitting applications",
								appPeriodSeconds);
	cmd.AddValue ("print",
								"Whether or not to print various informations",
								print);
	cmd.Parse (argc, argv);

	/**
	 * Enable Log
	 * ----------
	 * To do it, just remove the comments
	 */
	// Set up logging
	LogComponentEnable ("ComplexLorawanNetworkExample", LOG_LEVEL_ALL);
// LogComponentEnable("LoraChannel", LOG_LEVEL_INFO);
//  LogComponentEnable("LoraPhy", LOG_LEVEL_ALL);
// LogComponentEnable("EndDeviceLoraPhy", LOG_LEVEL_ALL);
// LogComponentEnable("GatewayLoraPhy", LOG_LEVEL_ALL);
// LogComponentEnable("LoraInterferenceHelper", LOG_LEVEL_ALL);
// LogComponentEnable("LoraMac", LOG_LEVEL_ALL);
// LogComponentEnable("EndDeviceLoraMac", LOG_LEVEL_ALL);
// LogComponentEnable("GatewayLoraMac", LOG_LEVEL_ALL);
// LogComponentEnable("LogicalLoraChannelHelper", LOG_LEVEL_ALL);
// LogComponentEnable("LogicalLoraChannel", LOG_LEVEL_ALL);
// LogComponentEnable("LoraHelper", LOG_LEVEL_ALL);
// LogComponentEnable("LoraPacketTracker", LOG_LEVEL_ALL);
// LogComponentEnable("LoraPhyHelper", LOG_LEVEL_ALL);
// LogComponentEnable("LoraMacHelper", LOG_LEVEL_ALL);
// LogComponentEnable("PeriodicSenderHelper", LOG_LEVEL_ALL);
// LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
// LogComponentEnable("LoraMacHeader", LOG_LEVEL_ALL);
// LogComponentEnable("LoraFrameHeader", LOG_LEVEL_ALL);
// LogComponentEnable("NetworkScheduler", LOG_LEVEL_ALL);
// LogComponentEnable("NetworkServer", LOG_LEVEL_ALL);
// LogComponentEnable("NetworkStatus", LOG_LEVEL_ALL);
// LogComponentEnable("NetworkController", LOG_LEVEL_ALL);

	
	/**
	 *  Setup
	 *=======
	 * @parblock
	 * Create the time value from the period and set a seed
	 *
	 * Decides how the devices are created
	 * ------------------------------------
	 * @endpar
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
	 * if( !FromFile ) 
	 *     Create devices random
	 * else
	 *     Import positions from file_users
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

	/// 
	Time appPeriod = Seconds (appPeriodSeconds);
	for (uint32_t i = 0; i < nRuns; i++) {
		uint32_t seed = randomSeed + i;
		SeedManager::SetSeed (seed);
	MobilityHelper mobility;
	if ( !FromFile ) {
	// Mobility Disco like
		mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
																	 "rho", DoubleValue (radius),
																	 "X", DoubleValue (0.0),
																	 "Y", DoubleValue (0.0));
	} else {
		Ptr<ListPositionAllocator> allocat = CreateObject<ListPositionAllocator> ();
		//Mobility Square Like
		std::ifstream infile(file_users);
		double a, b;
		while(infile >> a >> b)
		{
//			cout << "x: " << a << ", y: "<< b << endl;
			allocat->Add (Vector (a,b,0.0));
		}
		infile.close();
		mobility.SetPositionAllocator(allocat);
	}
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	 
		/**
		 * Create the channel
		 * ------------------
		 *  We decided to use **OkumuraHataPropagationLossModel**
		 */
	
		// Create the lora channel object
//			Ptr<OkumuraHataPropagationLossModel> loss = CreateObject<OkumuraHataPropagationLossModel> ();
		Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
		loss->SetPathLossExponent (3.76);
		loss->SetReference (1, 8.7);
		
		/**
		 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
		 * if(realisticChannelModel) {
		 *   Create the correlated shadowing component
		 *   Aggregate shadowing to the Okumura Hata loss
		 *   Add the effect to the channel propagation loss
		 * } 
		 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		 */
		if (realisticChannelModel)
			{
				// Create the correlated shadowing component
				Ptr<CorrelatedShadowingPropagationLossModel> shadowing = CreateObject<CorrelatedShadowingPropagationLossModel> ();
	
				// Aggregate shadowing to the logdistance loss
				loss->SetNext (shadowing);
	
				// Add the effect to the channel propagation loss
				Ptr<BuildingPenetrationLoss> buildingLoss = CreateObject<BuildingPenetrationLoss> ();
	
				shadowing->SetNext (buildingLoss);
			}
	
		Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();
	
		Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);
	
		/**
		 * Create the helpers
		 * ------------------
		 */
	
		/**
		 * ### Create the LoraPhyHelper ###
		 * Using the funcition  LoraPhyHelper()
		 */
		LoraPhyHelper phyHelper = LoraPhyHelper ();
		phyHelper.SetChannel (channel);
	
		/**
		 * ### Create the LoraMacHelper ###
		 * Using the funcition LoraMacHelper()
		 */
		LoraMacHelper macHelper = LoraMacHelper ();
	
		/**
		 * ### Create the LoraHelper ###
		 * Using the funcition LoraMacHelper()
		 */
		LoraHelper helper = LoraHelper ();
		helper.EnablePacketTracking ("performance"); // Output filename
		helper.EnableSimulationTimePrinting ();
	
		/// Create the NetworkServerHelper
		NetworkServerHelper nsHelper = NetworkServerHelper ();
	
		/// Create the ForwarderHelper
		ForwarderHelper forHelper = ForwarderHelper ();
		
		/// Create a set of nodes
		NodeContainer endDevices;
		if (!FromFile)
			endDevices.Create (nDevices);
		else
			endDevices.Create ( getFileSize(file_users) );
	
		/// Assign a mobility model to each node
		mobility.Install (endDevices);
	
		///  Make it so that nodes are at a certain height > 0 (1.2)
		for (NodeContainer::Iterator j = endDevices.Begin ();
				 j != endDevices.End (); ++j)
			{
				Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
				Vector position = mobility->GetPosition ();
				position.z = 1.2;
				mobility->SetPosition (position);
			}
	
		/// Create the LoraNetDevices of the end devices
		uint8_t nwkId = 54;
		uint32_t nwkAddr = 1864;
		Ptr<LoraDeviceAddressGenerator> addrGen = CreateObject<LoraDeviceAddressGenerator> (nwkId,nwkAddr);
	
		/// Create the LoraNetDevices of the end devices
		macHelper.SetAddressGenerator (addrGen);
		phyHelper.SetDeviceType (LoraPhyHelper::ED);
		macHelper.SetDeviceType (LoraMacHelper::ED);
		helper.Install (phyHelper, macHelper, endDevices);
	
		/// Now end devices are connected to the channel
	
		// Connect trace sources
		for (NodeContainer::Iterator j = endDevices.Begin ();
				 j != endDevices.End (); ++j)
			{
				Ptr<Node> node = *j;
				Ptr<LoraNetDevice> loraNetDevice = node->GetDevice (0)->GetObject<LoraNetDevice> ();
				Ptr<LoraPhy> phy = loraNetDevice->GetPhy ();
			}
	
		/*********************
		 *	Create Gateways	*
		 *********************/
	
		// Create the gateway nodes (allocate them uniformely on the disc)
		NodeContainer gateways;
		// Make it so that nodes are at a certain height > 0
		Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
//		FromFile = false;
	if ( !FromFile ) {
		gateways.Create (nGateways);
		if(nGateways == 1) {
			allocator->Add (Vector (0.0, 0.0, 15.0));
		} else if (nGateways == 2){
			allocator->Add (Vector (-1500.0, 000.0, 15.0));
			allocator->Add (Vector (1500.0, 000.0, 15.0));
		}else {
//			randomize();
			for(int i=1;i<= nGateways;i++)
			{
				allocator->Add (Vector (rand()%10000, rand()%10000, 15.0));
			}
		}
	} else {
		gateways.Create ( getFileSize(file_gateway) );
		// Get Gateway positions from File
		ifstream infile2(file_gateway);
		double gwa, gwb;
		while(infile2 >> gwa >> gwb)
		{
//			cout << "Gx: " << gwa*1000 << ", Gy: "<< gwb*1000 << endl;
//			allocator->Add (Vector (gwa,gwb,15.0));
			allocator->Add (Vector (gwa,gwb,15.0));
		}
		infile2.close();
	}
	mobility.SetPositionAllocator(allocator);
	mobility.Install (gateways);
	
		// Create a netdevice for each gateway
		phyHelper.SetDeviceType (LoraPhyHelper::GW);
		macHelper.SetDeviceType (LoraMacHelper::GW);
		helper.Install (phyHelper, macHelper, gateways);
	
		/**********************
		 *	Handle buildings	*
		 **********************/
	
		double xLength = 520;
		double deltaX = 128;
		double yLength = 256;
		double deltaY = 68;
		int gridWidth = 5 * radius / (xLength + deltaX);
		int gridHeight = 5 * radius / (yLength + deltaY);
		if (realisticChannelModel == false)
			{
				gridWidth = 0;
				gridHeight = 0;
			}
		Ptr<GridBuildingAllocator> gridBuildingAllocator;
		gridBuildingAllocator = CreateObject<GridBuildingAllocator> ();
		gridBuildingAllocator->SetAttribute ("GridWidth", UintegerValue (gridWidth));
		gridBuildingAllocator->SetAttribute ("LengthX", DoubleValue (xLength));
		gridBuildingAllocator->SetAttribute ("LengthY", DoubleValue (yLength));
		gridBuildingAllocator->SetAttribute ("DeltaX", DoubleValue (deltaX));
		gridBuildingAllocator->SetAttribute ("DeltaY", DoubleValue (deltaY));
		gridBuildingAllocator->SetAttribute ("Height", DoubleValue (6));
		gridBuildingAllocator->SetBuildingAttribute ("NRoomsX", UintegerValue (2));
		gridBuildingAllocator->SetBuildingAttribute ("NRoomsY", UintegerValue (4));
		gridBuildingAllocator->SetBuildingAttribute ("NFloors", UintegerValue (2));
//		gridBuildingAllocator->SetAttribute ("MinX", DoubleValue (-gridWidth * (xLength + deltaX) / 2 + deltaX / 2));
//		gridBuildingAllocator->SetAttribute ("MinY", DoubleValue (-gridHeight * (yLength + deltaY) / 2 + deltaY / 2));
		gridBuildingAllocator->SetAttribute ("MinX", DoubleValue(10));
		gridBuildingAllocator->SetAttribute ("MinY", DoubleValue(10));
		BuildingContainer bContainer = gridBuildingAllocator->Create (gridWidth * gridHeight);
	
		BuildingsHelper::Install (endDevices);
		BuildingsHelper::Install (gateways);
		BuildingsHelper::MakeMobilityModelConsistent ();
	
		// Print the buildings
		if (print)
			{
				std::ofstream myfile;
				myfile.open ("src/lorawan/examples/buildings.txt");
				std::vector<Ptr<Building> >::const_iterator it;
				int j = 1;
				for (it = bContainer.Begin (); it != bContainer.End (); ++it, ++j)
					{
						Box boundaries = (*it)->GetBoundaries ();
						myfile << "set object " << j << " rect from " << boundaries.xMin << "," << boundaries.yMin << " to " << boundaries.xMax << "," << boundaries.yMax << std::endl;
					}
				myfile.close ();
	
			}
	
		/**********************************************
		 *	Set up the end device's spreading factor	*
		 **********************************************/
	
		macHelper.SetSpreadingFactorsUp (endDevices, gateways, channel);
	
//		NS_LOG_DEBUG ("Completed configuration");
	
		/*********************************************
		 *	Install applications on the end devices	*
		 *********************************************/
	
		Time appStopTime = Seconds (simulationTime);
		PeriodicSenderHelper appHelper = PeriodicSenderHelper ();
		appHelper.SetPeriod (Seconds (appPeriodSeconds));
//		appHelper.SetPacketSize (23);
		appHelper.SetPacketSize (50);
		Ptr <RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> ("Min", DoubleValue (0), "Max", DoubleValue (10));
		ApplicationContainer appContainer = appHelper.Install (endDevices);
	
		appContainer.Start (Seconds (0));
		appContainer.Stop (appStopTime);
	
		/**************************
		 *	Create Network Server	*
		 ***************************/
	
		// Create the NS node
		NodeContainer networkServer;
		networkServer.Create (1);
	
		// Create a NS for the network
		nsHelper.SetEndDevices (endDevices);
		nsHelper.SetGateways (gateways);
		nsHelper.Install (networkServer);
	
		//Create a forwarder for each gateway
		forHelper.Install (gateways);
	
		/**********************
		 * Print output files *
		 *********************/
		if (print)
			{
				helper.PrintEndDevices (endDevices, gateways,
																"src/lorawan/examples/endDevices.dat");
			}
	
		////////////////
		// Simulation //
		////////////////
	
		Simulator::Stop (appStopTime + Hours (1));
//		AnimationInterface anim ("lorawan-netamin.xml");
//		anim.EnablePacketMetadata (true);
	
//		NS_LOG_INFO ("Running simulation...");
		Simulator::Run ();
	
		Simulator::Destroy ();
	
		///////////////////////////
		// Print results to file //
		///////////////////////////
//		NS_LOG_INFO ("Computing performance metrics...");
		helper.PrintPerformance (transientPeriods * appPeriod, appStopTime);
//		std::cout << getFileSize(file_gateway)<< std::endl;
	}
		return 0;
}
